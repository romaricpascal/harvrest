Harvrest
===

Pomodoro-like activity sessions that can carry on after the initial 25min are ellapsed.
Rest time is scaled to match active time, both after each session and after blocks of sessions.
Durations and number of sessions (for blocks and total) are configurable.

Audio
---

Sounds by Joao Janz:
- https://freesound.org/people/Joao_Janz/sounds/482653/
- https://freesound.org/people/Joao_Janz/sounds/504784/
